/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package zen_training_generic;

/**
 *
 * @author Lev
 */
public class Generic <T>{
	
	T variable;
	
	Generic(T v) {
		variable = v;
	}
	
	public void printVar() {
		System.out.println(variable);
	}

}

