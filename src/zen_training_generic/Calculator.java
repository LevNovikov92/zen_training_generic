/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package zen_training_generic;

import java.util.List;

/**
 *
 * @author Lev
 */
public class Calculator {
	
	public static Double summ(List<? extends Number> list) {
		Double summ = 0.0;
		for(Number num: list) {
			summ+=num.doubleValue();
		}
		return summ;
	}
}
