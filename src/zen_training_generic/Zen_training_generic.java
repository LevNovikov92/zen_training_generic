/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package zen_training_generic;

import java.util.ArrayList;

/**
 *
 * @author Lev
 */
public class Zen_training_generic {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        exercise_1();
        exercise_2();
        exercise_3();
    }
    
    private static void exercise_1() {
		Generic<String> gen = new Generic<>("generic test");
		gen.printVar();
		System.out.println("Exercise 1 complete");
		System.out.println();
    }
    
    private static void exercise_2() {
		ArrayList<Integer> IntList = new ArrayList<>();
		IntList.add(100);
		IntList.add(100);
		IntList.add(100);
		IntList.add(110);
		
		Double summ1 = Calculator.summ(IntList);
		System.out.println("Summ1 (int): " + summ1);
		
		ArrayList<Double> DoubleList = new ArrayList<>();
		DoubleList.add(10.0);
		DoubleList.add(10.0);
		DoubleList.add(10.0);
		DoubleList.add(11.0);
		
		Double summ2 = Calculator.summ(DoubleList);
		System.out.println("Summ2 (double): " + summ2);
		
		ArrayList<Number> MixedList = new ArrayList<>();
		MixedList.add(100);
		MixedList.add(100.0);
		MixedList.add(100);
		MixedList.add(110.0);
		
		Double summ3 = Calculator.summ(IntList);
		System.out.println("Summ3 (mixed): " + summ3);
		
		System.out.println("Exercise 2 complete");
		System.out.println();
    }
    
    private static void exercise_3() {
		Property<String> prop1 = new Property<>();
		prop1.setValue("Value 1");
		
		Property<Integer> prop2 = new Property<>();
		prop2.setValue(14);
		
		
		System.out.println("Property1 = " + prop1.getValue());
		System.out.println("Property2 = " + prop2.getValue());
		
		System.out.println("Exercise 3 complete");
		System.out.println();
    }
}
