/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package zen_training_generic;

/**
 *
 * @author Lev
 */
public class Property<T> {
	public T Value;
	
	public void setValue(T value) {
		Value = value;
	}
	
	public T getValue() {
		return Value;
	}
}

